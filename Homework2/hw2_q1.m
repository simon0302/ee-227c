%% initialize
clear; clc;
n = 10;
x1 = ones(10, 1);
Niter = 50;

fi = zeros(1, Niter);
fi(1, 1) = fx(x1);

xi = zeros(10, 1, Niter);
xi(:, :, 1) = x1;

lambda = 0.9912; % lambda between 0 and 1
affine_f = @(x) f_ix_wrapper(x, xi);
%% level method
% optimize over convex unit l-inf ball
ub = 1*ones(10, 1);
lb = - ub;

for i = 2:1:Niter
    disp(i);

    % 1. fi_m = solve min f(x) over G
    Ak_i = zeros(10, 10, i);
    bk_i = zeros(10, 1, i);

    f_i_max = cell(1, i);

    for j = 1:1:i

        values = zeros(1, 5);

        for k = 1:5
            %disp(k);
            [Ak, bk] = Akbk_fn(k, 10);
            values(:, k) = xi(:, :, j)'*Ak*xi(:, :, j) - bk'*xi(:, :, j);
        end

        [~, ind] = max(values);
        [Ak, bk] = Akbk_fn(ind, n); % finds Ak and bk corresponding to maximum
        
        Ak_i(:, :, j) = Ak;
        bk_i(:, :, j) = bk;
        
        f_i_max{j} = @(x) xi(:, :, j)'*Ak*xi(:, :, j) - bk'*xi(:, :, j) + (2*Ak*xi(:, :, j) - bk)'*(x - xi(:, :, j));
    end
    
    f_i_max_i = zeros(1, i); 
    for ll = 1:1:i
        f_i_max_i(:, ll) = f_i_max{ll}( xi(ll) );
    end
    
    [~, ind_f_i_max] = max(f_i_max_i);
    fi_fn = @(x) f_i_max{ind_f_i_max}(x); 
    [~, fi_m] = fmincon(fi_fn, xi(:, :, i), [], [], [], [], lb, ub);
    
    % 2. fi_p - done
    fi_p = min(fi);

    % 3. level set formation - done
    l_i = (1 - lambda)*fi_m + fi_p; 

    % 4. project x_i_p1 onto Q
    q_fn = @(y) norm(y - xi(:, :, i)); % projection norm objective fn
    
    % projection, designing NL constraint
    c_eq = @(x) 0; % no equality constraints
    c_ieq = @(x) fi_fn(x) - l_i;
    nl_constr = @(x) deal(c_eq(x), c_ieq(x));

    options = optimoptions("fmincon","MaxIterations",1e4,"MaxFunctionEvaluations",1e4);
    options = optimoptions(options,"EnableFeasibilityMode",true,...
    "SubproblemAlgorithm","cg");
    [x_i_p1, ~] = fmincon(q_fn, xi(:, :, i), [], [], [], [], lb, ub, nl_constr, options); 

    % updating
    if i ~= Niter
        fi(1, i+1) = fx(x_i_p1);
        xi(:, :, i+1) = x_i_p1;
    end
end
%
fprintf(1, "f* is: %0.4f\n x* is: \n", fi(end));
%% plotting
sub_gap = fi - fi(end)*ones(1, Niter);
figure();
plot(1:1:Niter, sub_gap, 'LineWidth', 1);
xlabel("Num. Iterations");
ylabel("f current - f*");
title("Rate of convergence of suboptimality gap v. best iterate");