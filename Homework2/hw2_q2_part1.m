% simon xu
% EE 227C hw 2, question 2, part 1
%% initialize
tol = 1e-6;

mu = 0;
var = 1;
n = 20;

% making A
A = normrnd(mu, sqrt(var), n);

f_fn = @(x) (A*x)'*(A*x);
grad_f = @(x) 2*(A'*A)*x;

x0 = ones(20, 1); % initial point
c = 10000;
d = 0.01;
%% part a - gradient descent
fcurr = intmax;
prev_f = 0;

%initializing GD
xt = x0;

i = 1;
fprintf(1, "Running part a...\n");
while abs(fcurr - prev_f) > tol
    prev_f = fcurr;
    
    lr = 0.01/sqrt(i); % learning rate
    xt_p1 = xt - lr*grad_f(xt);

    fcurr = f_fn(xt_p1);
    xt = xt_p1;
    
    if i == 1
        fi = fcurr;
    else
        fi = [fi fcurr];
    end
    i = i + 1;
end
fprintf(1, "done\n")
%% part b - AGD 
fcurr = 10;
prev_f = 0;

%initializing AGD
z0 = x0;

x_t_1 = x0;
z_t_1 = z0;

t = 1; % counter
fprintf(1, "Running part b...\n");
while abs(fcurr - prev_f) > tol
    prev_f = fcurr;
    
    if t < 4
        lambda_t = 1;
        gamma_t = 0;
    else
        lambda_t = 6/(t*(t-1));
        gamma_t = 2/t;
    end

    y_t_1 = (1 - gamma_t)*x_t_1 + gamma_t*z_t_1;
    z_t = z_t_1 - (gamma_t/(lambda_t*2*c))*grad_f(y_t_1);
    x_t = y_t_1 - 0.01*grad_f(y_t_1);

    fcurr = f_fn(x_t);
    x_t_1 = x_t;
    z_t_1 = z_t;

    if t == 1
        ft = fcurr;
    else
        ft = [ft fcurr];
    end
    t = t + 1;
end
fprintf(1, "done\n");
%% plotting GD and AGD
% plotting
figure();
loglog(1:1:i-1, fi' - fi(end)*ones(size(fi')), 'LineWidth', 1); % gradient descent
hold on;
loglog(1:1:t-1, ft' - ft(end)*ones(size(ft')), 'LineWidth', 1); % AGD
title("Rate of convergence v. iteration number");
xlabel("k"); ylabel("Rate of convergence of best iterate");
legend("GD", "AGD");
