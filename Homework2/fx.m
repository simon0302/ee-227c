function f = fx(x)
    values = zeros(1, 5);
    
    for k = 1:1:5
        [Ak, bk] = Akbk_fn(k, 10); % returns (n x n) Ak; (n x 1) bk
        values(:, k) = x'*Ak*x - bk'*x;
    end
    f = max(values);
end
