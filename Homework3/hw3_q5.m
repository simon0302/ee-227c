% simon xu
% ee 227c, homework 3
% question 5
clear; clc;
%% begin sim
n = 20;
t0 = 0;
beta = 1/2; % between 0 and 1/2
epsilon = 0.01;
MAXiter = 1e3;

x0 = 20*ones(n, 1); % chosen myself, so that x0 in domF

A = [eye(20); -eye(20)];
b = ones(40, 1);
nu = 1;
c = ones(n, 1);

tk = t0;
gamma = 0.01;

xk = x0;
for i = 1:1:MAXiter
    if tk >= (nu + (beta + sqrt(nu))*beta/(1-beta))/epsilon
        break;
    end
    
    norm_c = (beta + sqrt(nu))/i;
    tk_p1 = tk + gamma/norm_c;
    lambda_k = norm(tk_p1*c + gradF(xk));
    ek = (lambda_k^2)/(1+lambda_k);
    xk_p1 = xk - inv(hessianF(xk))*(tk_p1*c + gradF(xk))/(1+ek);

    % update
    tk = tk_p1;
    xk = xk_p1;
end
fprintf("x* is:\n");
fprintf("-%0.4f\n", xk);
fprintf("\nJ* is: \n");
fprintf("-%0.4f\n", c'*xk);
%%
function val = hessianF(xk)
    n = 20;

    A = [eye(n); -eye(n)];
    b = ones(2*n, 1);
    
    hessian_F_fn = 0;
    
    for i = 1:1:2*n
        hessian_F_fn = hessian_F_fn + A(i, :)'*A(i, :)./(b(i) - A(i, :)*xk)^2;
    end
    
    val = hessian_F_fn;
end

function val = gradF(xk)
    n = 20;
    A = [eye(n); -eye(n)];
    b = ones(2*n, 1);
    grad_F_fn = 0;
    
    for i = 1:1:2*n
        grad_F_fn = grad_F_fn + A(i, :)'./(b(i) - A(i, :)*xk);
    end

    val = grad_F_fn;
end
