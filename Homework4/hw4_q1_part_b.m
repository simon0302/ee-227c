% simon xu
% ee 227c, hw4
% problem 1, part b
%%
clear; clc;
x0 = zeros(10, 1);
epsil = [1, 0.1, 0.001, 0.005];
Nsim = 1e4;
Mf = 1;
tol = 1e-6;
%% begin simulation
for E = epsil
    fprintf(1, "Running epsilon = %0.3f\n", E);
    xk = x0;
    for k = 2:1:Nsim
        
        lambda_xk = lambda(xk, E);
        if lambda_xk <= tol
            fprintf(1, "ended sim at k = %d\n", k);
            disp(xk);
            break;
        end

        if k == Nsim
            fprintf("Too little iterations, no convergence yet\n");
        end
    
        eta = 1/(1 + Mf*lambda_xk);
        
        %update xk
        xk = xk - eta*inv(hessianf(xk))*gradf(xk, E);
    end
end
%% helper functions
function y = gradf(x, epsil)
    y = zeros(10, 1);
    for i = 1:1:10
        y(i) = (i/epsil) + 2*x(i)/(1-(x(i))^2);
    end
end

function y = hessianf(x)
    y = zeros(10, 10);
    for i = 1:1:10
        y(i, i) = 2*(1+(x(i))^2)/(1-(x(i))^2)^2;
    end
end

function y = lambda(x, epsil)
    gradf_val = gradf(x, epsil);
    y = sqrt(gradf_val'*hessianf(x)*gradf_val);
end
