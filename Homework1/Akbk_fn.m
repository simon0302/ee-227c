function [Ak,bk] = Akbk_fn(k,n)
    bk = zeros(n,1);
    for i = 1:1:n
        bk(i) = exp(i/k)*sin(i*k);

        for j = 1:1:n

            if i < j
                val = exp(i/j)*cos(i*j)*sin(k);
                Ak(i,j) = val;
                Ak(j,i) = val;
            end
        end
        Ak(i,i) = (i/10)*abs(sin(k))+ sum(abs(Ak(:, i)));
    end
end
