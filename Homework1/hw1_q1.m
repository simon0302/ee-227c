n = 10;
x1 = ones(10,1);
values = zeros(1, 5);

for k = 1:5

    [Ak, bk] = Akbk_fn(k, n); % returns n by n Ak; n by 1 bk
    
    values(k) = x1'*Ak*x1 - bk'*x1;
end

f_x1 = max(values);
fprintf(1, "f(x^1) is: %0.4d\n", f_x1);

%% part b
N_iter = 1e4;

C = 0.001;

n = 10;
x1 = ones(10,1);
values = zeros(1, 5);
fbest = 999999;
xbest = 0;


fi = zeros(1, N_iter);

for i = 1:N_iter    
    lr = C/sqrt(i); % learning rate
    
    for k = 1:5
        [Ak, bk] = Akbk_fn(k, n); % returns n by n Ak; n by 1 bk
        values(k) = x1'*Ak*x1 - bk'*x1;
    end

    [f_x1, ind] = max(values);
    fi(i) = f_x1;
    
    if f_x1 < fbest
        fbest = f_x1;
        xbest = x1;
    end
    
    %need to update x1
    [Ak, bk] = Akbk_fn(ind, n);
    g = 2*Ak*x1 - bk;
    x1 = x1 - lr*g;
end
%
fprintf(1, "f* is: %0.4f\n x* is: \n", fbest);
disp(xbest);

%plotting
sub_gap = fi - fbest*ones(1, N_iter);
figure();
loglog(sub_gap, 'LineWidth', 1);
xlabel("Num. Iterations");
ylabel("f current - f*");
title("Suboptimality Gap, C = 0.001 v. Iterations");
%% part c
N_iter = 1e2;
n = 10;
x1 = ones(10,1);
values = zeros(1, 5);

fopt = -0.841356986375302; % fopt = fbest from part b
fbest = 999999;
xbest = 0;

fi = zeros(1, N_iter);

for i = 1:N_iter    
    
    for k = 1:5
        [Ak, bk] = Akbk_fn(k, n); % returns n by n Ak; n by 1 bk
        values(k) = x1'*Ak*x1 - bk'*x1;
    end

    [f_x1, ind] = max(values);
    fi(i) = f_x1;
    
    if f_x1 < fbest
        fbest = f_x1;
        xbest = x1;
    end
    
    %need to update x1
    [Ak, bk] = Akbk_fn(ind, n);
    g = 2*Ak*x1 - bk; %g(xi)
    %disp(f_x1);
    lr = (f_x1 - fopt)/norm(g,2)^2;
    
    %(f_x1 - fopt)/norm(g, 2);
    %disp(lr);
    x1 = x1 - lr*g;
end

fprintf(1, "f* is: %0.4f\n x* is: \n", fbest);
disp(xbest);

%plotting
sub_gap = fi - fbest*ones(1, N_iter);

figure();
loglog(sub_gap, 'LineWidth', 1);
xlabel("Num. Iterations");
ylabel("f current - f*");
title("Suboptimality Gap, Polyak Step Size  v. Iterations");